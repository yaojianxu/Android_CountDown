﻿

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sikao.app.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sun on 2016/8/2.
 * 倒计时的自定义控件
 */
public class CountDownLinearLayout extends LinearLayout {
    private TextView tvMonthOne;
    private TextView tvMonthTwo;
    private TextView tvDayOne;
    private TextView tvDayTwo;

    private TextView tvHourOne;
    private TextView tvHourTwo;
    private TextView tvMinuteOne;
    private TextView tvMinuteTwo;
    private TextView tvSecondOne;
    private TextView tvSecondTwo;
    private boolean isRun = false;

    public CountDownLinearLayout(Context context) {
        super(context);
    }

    private int recLen = 11;
    Timer timer =  new Timer() ;

    public CountDownLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_countdown_ll, this);


        tvMonthOne = (TextView) findViewById(R.id.tv_month_one);
        tvMonthTwo = (TextView) findViewById(R.id.tv_month_two);
        tvDayOne = (TextView) findViewById(R.id.tv_day_one);
        tvDayTwo = (TextView) findViewById(R.id.tv_day_two);

        tvHourOne = (TextView) findViewById(R.id.tv_hour_one);
        tvHourTwo = (TextView) findViewById(R.id.tv_hour_two);
        tvMinuteOne = (TextView) findViewById(R.id.tv_minute_one);
        tvMinuteTwo = (TextView) findViewById(R.id.tv_minute_two);
        tvSecondOne = (TextView) findViewById(R.id.tv_second_one);
        tvSecondTwo = (TextView) findViewById(R.id.tv_second_two);

    }
        public void startCountDown(Long  endTime){
                    CountDownLinearLayout.endTime =endTime;
            if (!isRun) {
                timer.schedule(task, 1000, 1000);
            }
                   isRun = true;
        }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:

                        tvMonthOne.setText(month_one);
                        tvMonthTwo.setText(month_two);
                        tvDayOne.setText(day_one);
                        tvDayTwo.setText(day_two);

                        tvHourOne.setText(hour_one);
                        tvHourTwo.setText(hour_two);
                        tvMinuteOne.setText(minute_one);
                        tvMinuteTwo.setText(minute_two);
                        tvSecondOne.setText(second_one);
                        tvSecondTwo.setText(second_two);

                     if ("0".equals(month_one)&&"0".equals(month_two)&&"0".equals(day_one)&&"0".equals(day_two)
                             &&"0".equals(hour_one)&&"0".equals(hour_two)&&"0".equals(minute_one)&&"0".equals(minute_two)&&"0".equals(second_one)&&"0".equals(second_two)) {//都是0的话，就停止倒计时
                        timer.cancel();
                    }
            }
        }
    };

    TimerTask task = new TimerTask() {
        @Override
        public void run() {
              //diff--;
            //这里不断调用差值 getDifference();1000*60*60*24*15+1000*60*60*3+1000*60*50+1000*18+999
               long diff=endTime-System.currentTimeMillis()/1000;
            if(diff<0){
                diff=0;
            }
            getDifference(diff);

            Message message = new Message();
            message.what = 1;
            handler.sendMessage(message);
        }
    };

    public void stopTimer(){
        if (isRun){
            task.cancel();
            timer.cancel();
        }
    }

    public CountDownLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

   /* public CountDownLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }*/


    private final static long yearLevelValue = 365 * 24 * 60 * 60 ;
    private final static long monthLevelValue = 30 * 24 * 60 * 60 ;
    private final static long dayLevelValue = 24 * 60 * 60 ;
    private final static long hourLevelValue = 60 * 60 ;
    private final static long minuteLevelValue = 60 ;
    private final static long secondLevelValue = 1;
    public  static long endTime=0;
    private String month_one = "0";
    private String month_two = "0";
    private String day_one = "0";
    private String day_two = "0";
    private String hour_one = "0";
    private String hour_two = "0";
    private String minute_one = "0";
    private String minute_two = "0";
    private String second_one = "0";
    private String second_two = "0";

    public String getDifference(long nowTime, long targetTime) {//目标时间与当前时间差
        long period = targetTime - nowTime;
        return getDifference(period);
    }

    private String getDifference(long period) {//根据秒差计算时间差
        String result = null;


        /*******计算出时间差中的年、月、日、天、时、分、秒*******/
        int year = getYear(period);
        int month = getMonth(period - year * yearLevelValue);
        int day = getDay(period - year * yearLevelValue - month * monthLevelValue);
        int hour = getHour(period - year * yearLevelValue - month * monthLevelValue - day * dayLevelValue);
        int minute = getMinute(period - year * yearLevelValue - month * monthLevelValue - day * dayLevelValue - hour * hourLevelValue);
        int second = getSecond(period - year * yearLevelValue - month * monthLevelValue - day * dayLevelValue - hour * hourLevelValue - minute * minuteLevelValue);


        result = year + "年" + month + "月" + day + "天" + hour + "时" + minute + "分" + second + "秒";
        //拆分数据用于填充textview
        if((month+"").length()>1){
         month_one=(month+"").substring(0,1);
         month_two=(month+"").substring(1,2);
        }else{
            month_one="0";
            month_two=month+"";
        }

        if((day+"").length()>1){
            day_one=(day+"").substring(0,1);
            day_two=(day+"").substring(1,2);
        }else{
            day_one="0";
            day_two=day+"";
        }

        if((hour+"").length()>1){
            hour_one=(hour+"").substring(0,1);
            hour_two=(hour+"").substring(1,2);
        }else{
            hour_one="0";
            hour_two=hour+"";
        }

        if((minute+"").length()>1){
            minute_one=(minute+"").substring(0,1);
            minute_two=(minute+"").substring(1,2);
        }else{
            minute_one="0";
            minute_two=minute+"";
        }

        if((second+"").length()>1){
            second_one=(second+"").substring(0,1);
            second_two=(second+"").substring(1,2);
        }else{
            second_one="0";
            second_two=second+"";
        }


        return result;
    }

    public int getYear(long period) {
        return (int) (period / yearLevelValue);
    }

    public int getMonth(long period) {
        return (int) (period / monthLevelValue);
    }

    public int getDay(long period) {
        return (int) (period / dayLevelValue);
    }

    public int getHour(long period) {
        return (int) (period / hourLevelValue);
    }

    public int getMinute(long period) {
        return (int) (period / minuteLevelValue);
    }

    public int getSecond(long period) {
        return (int) (period / secondLevelValue);
    }

}
