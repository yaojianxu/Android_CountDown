#Android_CountDown
### 一个好看的Android倒计时控件。轻松搞定倒计时，避免造轮子
#### 使用说明
1、在项目中引入CountDownLinearLayout.java和布局文件以及样式文件，注意styles.xml只复制需要的style到你自己的styles.xml中就可以
2、在要展示的布局中引入倒计时控件
      < CountDownLinearLayout
                android:id="@+id/cdll"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
            >
   实例化后在Activity中调用(endTime为long)
   countDownLinearLayout.startCountDown(endTime);
3、为避免内存泄露，在Activity退出销毁的时候调用countDownLinearLayout.stopTimer();来关闭倒计时线程
          @Override
	protected void onDestroy() {
		countDownLinearLayout.stopTimer();
		super.onDestroy();
	}

#### 效果展示
![输入图片说明](http://git.oschina.net/uploads/images/2016/1020/145938_421b14c6_355187.png "倒计时效果展示")